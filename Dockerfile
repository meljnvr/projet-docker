FROM php:8.2.17-apache
# ARG COMPOSER_ALLOW_SUPERUSER=1
ENV APP_ENV=dev
ENV APP_DEBUG=1
ENV COMPOSER_ALLOW_SUPERUSER=1
# Install system dependencies
RUN apt-get update && apt-get install -y --no-install-recommends git curl zip unzip libldap2-dev libldap-common libfreetype6-dev \
 libjpeg62-turbo-dev libpng-dev locales pkg-config libonig-dev libicu-dev
# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring intl

RUN echo "ServerName localhost" | tee -a /etc/apache2/apache2.conf
COPY ./.docker/apache /etc/apache2/sites-enabled
RUN a2enmod rewrite && service apache2 restart
# Install composer
RUN curl -sSk https://getcomposer.org/installer | php -- --disable-tls --version=2.7.2 \
 && mv composer.phar /usr/local/bin/composer
WORKDIR /var/www/html/
COPY . .
RUN composer install --no-interaction --no-progress