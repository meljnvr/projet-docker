DOCKER_COMPOSE = docker-compose
DOCKER = docker
EXEC_BASE = $(DOCKER) exec -it demo_php
EXEC_PHP = $(EXEC_BASE) php
SYMFONY = $(EXEC_PHP) bin/console
COMPOSER = $(EXEC_BASE) composer
APP_PATH = $(shell pwd)/
## Project
## -------
d-build:
	echo "d-build>"
	@$(DOCKER_COMPOSE) pull --parallel --quiet --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull
 
d-start: ## Start the project
	echo "d-start>"
	$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

d-kill:
	echo "d-kill>"
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

d-stop: ## Stops the project
	echo "d-stop>"
	$(DOCKER_COMPOSE) stop

d-down: ## Stops the project and containers
	echo "d-down>"
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

d-install: ## Installs and starts a fresh project
d-install: d-build d-start purge-all install-all
d-reset: ## Stops and starts a fresh install of the project
d-reset: d-kill d-install
d-restart: d-down d-start

d-enter: ## enter in container
	$(EXEC_BASE) bash

.PHONY: d-build d-start d-kill d-stop d-install d-reset d-restart d-enter

# rules based on files
$(APP_PATH)composer.lock: $(APP_PATH)composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

vendor: $(APP_PATH)composer.lock
	$(COMPOSER) install --no-plugins --no-scripts --no-interaction

saas:
	$(SYMFONY) sass:build

.PHONY: vendor saas